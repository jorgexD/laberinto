﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float force;

	public float vel_scale;

	Vector3 temp;

	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {

		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
		Vector3 vector = new Vector3 (h, 0.5f, v);

		Rigidbody rb = GetComponent<Rigidbody> ();
		rb.AddForce (vector * force * Time.deltaTime);

		temp = transform.localScale;

		temp.x += (Time.deltaTime/vel_scale);
		temp.y += (Time.deltaTime/vel_scale);
		temp.z += (Time.deltaTime/vel_scale);

		transform.localScale = temp;
	
	}

	void OnTriggerEnter( Collider obj){

		if (obj.gameObject.tag == "Enlace") {
		
			if( Application.loadedLevelName == "Main"){

				Application.LoadLevel("Scene1");
			}
			else{

				Application.LoadLevel("Main");
			}
		
		}
	}
	
}
