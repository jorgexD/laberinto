﻿using UnityEngine;
using System.Collections;

public class ControllerCamara : MonoBehaviour {

	public GameObject player;
	private Vector3 posIni;

	// Use this for initialization
	void Start () {

		posIni = this.transform.position - player.transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {

		this.transform.position = player.transform.position + posIni;
	
	}
}
